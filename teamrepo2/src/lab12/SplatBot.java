package lab12;

//TO TEST THIS SPLATBOT, RUN THE MAIN.JAVA FILE WITH SPLATBOT.CLASS AS ONE OF THE PARAMETERS TO THE SPLATTER CONSTRUCTOR!!!!!!!!!!!!!!
import splatbot.Action;
import splatbot.Cell;
import splatbot.SplatBotColor;

public class SplatBot {
	private SplatBotColor _color = null;
	private Cell _ourCellColor = null;
	private Cell _enemyCellColor = null;

	private int _repeatCount = 0;
	private Cell _lastLeft = Cell.NEUTRAL;
	private Cell _lastFront = Cell.NEUTRAL;
	private Cell _lastRight = Cell.NEUTRAL;

	public SplatBot(SplatBotColor color) {
		_color = color;
		if (color.equals(SplatBotColor.BLUE)) {
			_ourCellColor = Cell.BLUE;
			_enemyCellColor = Cell.RED;
		} else {
			_ourCellColor = Cell.RED;
			_enemyCellColor = Cell.BLUE;
		}
		System.out.println("[OK] Finsihed Initilizing KadeBot v2.04");
	}

	public Action getAction(Cell left, Cell front, Cell right) {
		boolean SomethingInFront = front.equals(Cell.WALL) || front.equals(Cell.ROCK) || front.equals(Cell.BLUE_ROBOT)
				|| front.equals(Cell.RED_ROBOT);
		boolean SomethingToRight = right.equals(Cell.WALL) || right.equals(Cell.ROCK) || right.equals(Cell.BLUE_ROBOT)
				|| right.equals(Cell.RED_ROBOT);
		boolean SomethingToLeft = left.equals(Cell.WALL) || left.equals(Cell.ROCK) || left.equals(Cell.BLUE_ROBOT)
				|| left.equals(Cell.RED_ROBOT);

		boolean enemyTileInFront = front.equals(_enemyCellColor);
		boolean enemyTileToRight = right.equals(_enemyCellColor);
		boolean enemyTileToLeft = left.equals(_enemyCellColor);

		boolean emptyTileInFront = front.equals(Cell.NEUTRAL);
		boolean emptyTileToRight = right.equals(Cell.NEUTRAL);
		boolean emptyTileToLeft = left.equals(Cell.NEUTRAL);

		// Detect stalls and correct
		if (left.equals(_lastLeft) && right.equals(_lastRight) && front.equals(_lastFront)) {
			_repeatCount += 1;
			if (_repeatCount >= 20) {
				if (Math.random() <0.5) {
					System.out.println("[CRITICAL] Stall Detected: Sending splat!");
					return Action.SPLAT; //sometimes it might be fun to just send a splat, instead of correcting
				}
				if (_repeatCount % 2 == 0) {
					System.out.println("[CRITICAL] Stall Detected: Attempting to correct automatically!");
					return decideDirection(!SomethingToLeft, !SomethingInFront, !SomethingToRight, true); //auto correction looks for paths out
				} else if (_repeatCount % 2 == 1) {
					System.out.println("[CRITICAL] Stall Detected: Trying to turn around!");
					return Action.TURN_LEFT; //sometimes auto-correct thinks it finds a spot, but the other splatbot is trying to move there as well, so just turn instead.
				}
			}
		} else {
			_repeatCount = 0;
		}
		_lastLeft = left;
		_lastRight = right;
		_lastFront = front;

		Action tmpAction = null;
		// check if there is an enemy tile here and return action
		// (example: if enemyTileToRight is true, return turn right)
		tmpAction = decideDirection(enemyTileToLeft, enemyTileInFront, enemyTileToRight, false);
		if (tmpAction != null) {
			return tmpAction;
		}

		// check if there is an empty tile here and return action
		tmpAction = decideDirection(emptyTileToLeft, emptyTileInFront, emptyTileToRight, false);
		if (tmpAction != null) {
			return tmpAction;
		}

		// you can leave this code how it is.
		tmpAction = decideDirection(!SomethingToLeft, !SomethingInFront, !SomethingToRight, true);
		return tmpAction;
	}

	private Action decideDirection(boolean left, boolean front, boolean right, boolean lastResort) {// true=can go
																									// there,
																									// false=can't go
																									// there
		if (!front) {
			if (right && left) {
				if (Math.random() > .5) { // nothing to the right and left, so pick a random direction
					return Action.TURN_RIGHT;
				} else {
					return Action.TURN_LEFT;
				}
			}
			if (right) { // if there is nothing in that direction, go that way
				return Action.TURN_RIGHT;
			}
			if (left) { // if there is nothing in that direction, go that way
				return Action.TURN_LEFT;
			}
			if (lastResort) {
				return Action.TURN_LEFT;
			} // can't go left or right, so start turning around
		} else {
			if (Math.random() < 0.1) { // randomly turn directions when there is nothing there
				if (right && left) {
					if (Math.random() > .5) { // nothing to the right and left, so pick a random direction
						return Action.TURN_RIGHT;
					} else {
						return Action.TURN_LEFT;
					}
				}
				if (right) { // if there is nothing in that direction, go that way
					return Action.TURN_RIGHT;
				}
				if (left) { // if there is nothing in that direction, go that way
					return Action.TURN_LEFT;
				}
			}
			return Action.MOVE_FORWARD; // nothing in front of us, so go forward
		}

		return null;
	}

	public void survey(Cell[][] map) {

	}
}